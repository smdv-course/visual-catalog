# Visual Catalog

Small examples of data driven graphics -- to be used as starting points for static graphics to be further refined through Illustrator.
[Viewable here](https://smdv-course.gitlab.io/visual-catalog/)


# Getting started

For ease of use we suggest you use [`live-server`](https://www.npmjs.com/package/live-server) to help you see your changes reflected immediately in your browser.

If you have live-server installed using it is as simple as opening terminal, navigating to the location of you project and typing `live-server`


# Setting up a Linux / Mac for editing the examples

 * Install Node.js
 * Open up your console / Terminal app
 * Install live-server `npm install -g live-server` (if you see errors you may need to try `sudo npm install -g live-server` which will then prompt you for your password)
 * Make sure Git is installed, type `git` into the command line. If it's not installed your Mac should take over and install it for you.



## Licence

This software is released by SciamLab under the [MIT licence](http://opensource.org/licenses/MIT)

## TODO
